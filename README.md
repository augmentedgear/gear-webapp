# Installation

This project uses MeteorJS.

To install, please follow these steps:

* Install [Meteor](https://www.meteor.com/install)
* Open a terminal in the app's main folder
* Run the command `meteor` to start the server
* In a desktop browser, access the app via <http://localhost:3000>

# Usage
