# URLs

http://ipaddress:3000

For development, it is http://10.11.17.121:3000

On the real server, we will use a domain name without a port.

## Guitar parts

* /product/guitar/:productid/tuning
* /product/guitar/:productid/neck
* /product/guitar/:productid/pickups
* /product/guitar/:productid/controls
* /product/guitar/:productid/body

## Guitar details

* /product/guitar/:productid/details
* /salesrep/guitars

Linked to the detail button on the lower left

## Product ids

The product ids will be one of these:

* danelectrodc59
* ...

## Sample

http://10.11.17.121:3000/product/guitar/danelectrodc59/tuning
