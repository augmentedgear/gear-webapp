module.exports = {
  parserOptions: {
    parser: 'babel-eslint',
    sourceType: 'module',
    ecmaVersion: 2017
  },
  extends: [
    'standard', 'standard-react'
  ],
  env: {
    node: true,
    browser: true,
    mocha: true
  },
  rules: {
    'comma-dangle': 0,
    'jsx-quotes': [
      "error", "prefer-double"
    ],
    'no-return-assign': 0,
    'no-trailing-spaces': 0,
    'react/react-in-jsx-scope': 0,
    semi: [
      2, "always"
    ],
    'react/prop-types': 0,
    'react/jsx-tag-spacing': 0,
    'space-before-function-paren': 0
  },
  globals: {
    "Cordova": false,
    "Npm": false,
    "Package": false
  }
}
