// Configuration file for https://github.com/zodern/meteor-up

module.exports = {
  servers: {
    one: {
      host: 'demo.gearhunt.io',
      username: 'ubuntu',
      pem: '../certs/guzz-meteor-ec2-alpha.pem'
    }
  },

  proxy: {
    domains: 'demo.gearhunt.io',
    ssl: {
      letsEncryptEmail: 'tom.brueckner@gmx.net',
      forceSSL: true
    }
  },

  meteor: {
    name: 'gearhunt',
    path: '../../',

    servers: {
      one: {}
    },

    buildOptions: {
      // set serverOnly: false if want to build mobile apps when deploying
      serverOnly: false,
      // debug: true,

      server: 'https://demo.gearhunt.io' // your app url for mobile app access (optional)
    },

    env: {
      ROOT_URL: 'https://demo.gearhunt.io',
      MONGO_URL:
        'mongodb://meteor:adbMx5qHqWzQnfUn@lamppost.16.mongolayer.com:10499,lamppost.3.mongolayer.com:10791/gear?replicaSet=set-56991fc6edf2316dff001321',
      MONGO_OPLOG_URL:
        'mongodb://meteor:adbMx5qHqWzQnfUn@lamppost.16.mongolayer.com:10499,lamppost.3.mongolayer.com:10791/local?authSource=gear&replicaSet=set-56991fc6edf2316dff001321'
    },

    docker: {
      // change to 'kadirahq/meteord' if your app is not using Meteor 1.4
      // image: 'abernix/meteord:node-8-base',
      image: 'abernix/meteord:base'
      // imagePort: 80,  (default: 80, some images EXPOSE different ports)
    },

    // This is the maximum time in seconds it will wait
    // for your app to start
    // Add 30 seconds if the server has 512mb of ram
    // And 30 more if you have binary npm dependencies.
    deployCheckWaitTime: 60,

    // Show progress bar while uploading bundle to server
    // You might need to disable it on CI servers
    enableUploadProgressBar: true
  }

  // mongo: {
  //   port: 27017,
  //   version: '3.4.1',
  //   servers: {
  //     one: {}
  //   }
  // }
};
