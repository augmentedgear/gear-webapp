import {
  artistsCollection,
  productsCollection,
  releasesCollection,
  tracksCollection
} from 'meteor/gear:collections';

import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { sevenDigital } from 'meteor/gear:api-7digital';

// This caches the artist, release and track data from 7digital for all products
// Kinda hacky implementation without optimizations, but it's a hackathon...

Meteor.startup(function() {
  const existingTracks = tracksCollection.find().count();
  if (existingTracks > 0) {
    return;
  }
  console.log('Retrieving releases and tracks related to products from 7digital');
  const products = productsCollection.find().fetch();
  _.each(products, ({ _id: productId, artists }) => {
    _.each(artists, name => {
      console.log('getting tracks for ' + name);
      const artist = sevenDigital.getFirstArtist(name);
      const artistId = artist.id;
      // enrich product collection by artist ids to avoid too many database calls
      productsCollection.update(
        { _id: productId },
        {
          $addToSet: {
            artistIds: artistId
          }
        }
      );
      artistsCollection.upsert({ _id: artistId }, { $set: _.omit(artist, 'id') });
      const releases = sevenDigital.getReleases(artistId);
      _.each(releases, release => {
        const releaseId = release.id;
        releasesCollection.upsert({ _id: releaseId }, { $set: _.omit(release, 'id') });
        const tracks = sevenDigital.getTrackListing(releaseId);
        _.each(tracks, track => {
          tracksCollection.upsert({ _id: track.id }, { $set: _.omit(track, 'id') });
        });
      });
    });
  });
});
