Package.describe({
  name: 'gear:crawler',
  version: '0.0.1',
  summary: 'Cache crawler to get tracks for artists linked to products'
});

Package.onUse(function(api) {
  api.use(['ecmascript']);
  api.use('gear:collections');
  api.use('gear:api-7digital');
  api.mainModule('main.server.js', 'server');
});
