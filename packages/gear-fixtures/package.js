Package.describe({ name: 'gear:fixtures', version: '0.0.1', summary: 'Preload data' });

Package.onUse(function(api) {
  api.use(['ecmascript', 'underscore']);
  api.use('gear:collections');
  api.addFiles('preload.js', 'server');
});
