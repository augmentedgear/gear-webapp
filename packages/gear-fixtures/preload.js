import { analyticsCollection, productsCollection } from 'meteor/gear:collections';

import { _ } from 'meteor/underscore';
import products from './products.json';

_.each(products, (product, _id) => {
  productsCollection.upsert(
    {
      _id
    },
    {
      $set: product
    }
  );
});

const analytics = analyticsCollection.findOne({});

if (!analytics) {
  analyticsCollection.upsert(
    {
      _id: 'analytics'
    },
    {
      $set: {
        'guitars.total': 200
      }
    }
  );
}
