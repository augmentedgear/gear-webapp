import Chart from 'chart.js';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';

const drawChart = function(guitars) {
  const ctx = document.getElementById('chart').getContext('2d');
  const myChart = new Chart(ctx, {
    type: 'bar',
    data: {
      labels: ['Guitars', 'Keys', 'Drums', 'Recording', 'Light', 'PA'],
      datasets: [
        {
          label: 'Visits in March 2018',
          data: [guitars, 150, 129, 98, 77, 32],
          backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(255, 206, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(255, 159, 64, 0.2)'
          ],
          borderColor: [
            'rgba(255,99,132,1)',
            'rgba(54, 162, 235, 1)',
            'rgba(255, 206, 86, 1)',
            'rgba(75, 192, 192, 1)',
            'rgba(153, 102, 255, 1)',
            'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
        }
      ]
    },
    options: {
      legend: {
        display: false
      },
      scales: {
        yAxes: [
          {
            ticks: {
              beginAtZero: true
            }
          }
        ]
      }
    }
  });
};

Template.dashboard.onRendered(function() {
  Meteor.call('analytics:getStatistics', 'guitars', (error, result) => {
    drawChart(result);
  });
});
