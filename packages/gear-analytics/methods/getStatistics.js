import { Meteor } from 'meteor/meteor';
import { analyticsCollection } from 'meteor/gear:collections';
import { check } from 'meteor/check';

Meteor.methods({
  'analytics:getStatistics': function(category) {
    check(category, String);
    const stats = analyticsCollection.findOne({ _id: 'analytics' });
    if (!stats) {
      return 0;
    }
    const result = (stats[category] || {}).total || 0;
    return result;
  }
});
