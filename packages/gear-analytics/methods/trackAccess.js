import { Meteor } from 'meteor/meteor';
import { analyticsCollection } from 'meteor/gear:collections';
import { check } from 'meteor/check';

Meteor.methods({
  'analytics:trackAccess': function(category, productId, part) {
    check(productId, String);
    check(category, String);
    check(part, String);
    const $inc = {};
    $inc[`${category}.${part}`] = 1;
    $inc[`${category}.total`] = 1;
    analyticsCollection.upsert(
      {
        _id: 'analytics'
      },
      {
        $inc
      }
    );
  }
});
