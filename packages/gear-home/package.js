Package.describe({ name: 'gear:home', version: '0.0.1', summary: 'Home page' });

Package.onUse(function(api) {
  api.use(['templating', 'ecmascript']);
  api.use('fourseven:scss@4.5.4');
  api.use(['kadira:flow-router@2.11.0', 'kadira:blaze-layout@2.3.0']);

  addTemplates(api, ['home']);
  api.addFiles('routes/home.js');
  // api.addAssets('images/home.background.png', 'client');
});

function addTemplates(api, templates) {
  for (var t in templates) {
    var path = 'templates/' + templates[t];
    var files = [path + '.html', path + '.js', path + '.scss'];
    api.addFiles(files, 'client');
  }
}
