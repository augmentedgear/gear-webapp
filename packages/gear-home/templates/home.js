/* global SearchSongsWidget */

const searchHandler = song => {
  FlowRouter.go(`/song/${song._id}`);
};

Template.home.onRendered(function() {
  // SearchSongsWidget.init({onSelect: searchHandler, onAutocomplete: searchHandler});
});
