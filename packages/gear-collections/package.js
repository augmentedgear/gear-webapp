Package.describe({ name: 'gear:collections', version: '0.0.1', summary: 'Database collections' });

Package.onUse(function(api) {
  api.use(['mongo', 'ecmascript']);
  api.use('gear:app-main');
  api.mainModule('main.js');
});
