import analyticsCollection from './collections/analytics';
import artistsCollection from './collections/artists';
import productsCollection from './collections/products';
import releasesCollection from './collections/releases';
import tracksCollection from './collections/tracks';

export {
  analyticsCollection,
  artistsCollection,
  productsCollection,
  releasesCollection,
  tracksCollection
};
