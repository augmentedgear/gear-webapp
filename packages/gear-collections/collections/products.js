import { Meteor } from 'meteor/meteor';

const productsCollection = new Meteor.Collection('products');

export default productsCollection;
