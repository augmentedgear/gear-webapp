import { Meteor } from 'meteor/meteor';

const artistsCollection = new Meteor.Collection('artists');

export default artistsCollection;
