import { Meteor } from 'meteor/meteor';

const analyticsCollection = new Meteor.Collection('analytics');

export default analyticsCollection;
