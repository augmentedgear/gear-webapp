import { Meteor } from 'meteor/meteor';

const releasesCollection = new Meteor.Collection('releases');

export default releasesCollection;
