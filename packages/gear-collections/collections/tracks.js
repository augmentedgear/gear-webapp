import { Meteor } from 'meteor/meteor';

const tracksCollection = new Meteor.Collection('tracks');

export default tracksCollection;
