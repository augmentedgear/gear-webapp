Package.describe({ name: 'gear:api-cloudinary', version: '0.0.1', summary: 'Cloudinary API' });

Package.onUse(function(api) {
  api.use(['ecmascript']);
  api.mainModule('main.server.js', 'server');
});
