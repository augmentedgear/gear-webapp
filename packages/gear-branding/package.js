Package.describe({ name: 'gear:branding', version: '0.0.1', summary: 'Brand name and logo' });

Package.onUse(function(api) {
  api.use(['templating', 'ecmascript', 'underscore']);
  api.use('fourseven:scss@4.5.4');
  api.use(['kadira:flow-router@2.11.0', 'kadira:blaze-layout@2.3.0']);

  addTemplates(api, ['logo']);
});

function addTemplates(api, templates) {
  for (var t in templates) {
    var path = 'templates/' + templates[t];
    var files = [path + '.html', path + '.js', path + '.scss'];
    api.addFiles(files, 'client');
  }
}
