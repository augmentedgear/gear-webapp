import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { FlowRouter } from 'meteor/kadira:flow-router';

FlowRouter.route('/salesrep/:dept', {
  name: 'contact',
  action: function() {
    BlazeLayout.render('layout', { mainCanvas: 'contact' });
  }
});
