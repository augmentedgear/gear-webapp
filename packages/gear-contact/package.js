Package.describe({ name: 'gear:contact', version: '0.0.1', summary: 'Contact page' });

Package.onUse(function(api) {
  api.use(['templating', 'ecmascript']);
  api.use('fourseven:scss@4.5.4');
  api.use(['kadira:flow-router@2.11.0', 'kadira:blaze-layout@2.3.0']);
  addTemplates(api, ['contact']);
  api.addFiles('routes/contact.js');
});

function addTemplates(api, templates) {
  for (var t in templates) {
    var path = 'templates/' + templates[t];
    var files = [path + '.html', path + '.js', path + '.scss'];
    api.addFiles(files, 'client');
  }
}
