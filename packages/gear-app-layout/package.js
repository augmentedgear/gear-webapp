Package.describe({ name: 'gear:app-layout', version: '0.0.1', summary: 'Main app layout' });

Package.onUse(function(api) {
  api.use(['templating', 'ecmascript']);
  api.use('fourseven:scss@4.5.4');
  api.use(['kadira:flow-router@2.11.0', 'kadira:blaze-layout@2.3.0']);

  addTemplates(api, ['layout']);
});

function addTemplates(api, templates) {
  for (var t in templates) {
    var path = 'templates/' + templates[t];
    var files = [path + '.html', path + '.js', path + '.scss'];
    api.addFiles(files, 'client');
  }
}
