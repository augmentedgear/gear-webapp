import { productsCollection, tracksCollection } from 'meteor/gear:collections';

import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { check } from 'meteor/check';

Meteor.methods({
  'product:tracksForProductId': function(productId) {
    check(productId, String);
    const product = productsCollection.findOne({ _id: productId });
    const tracks = [];
    _.each(product.artistIds, artistId => {
      const artistTracks = tracksCollection.find({ 'artist.id': artistId }, { limit: 1 }).fetch();
      tracks.push(_.uniq(artistTracks, item => item.title));
    });
    return tracks;
  }
});
//
