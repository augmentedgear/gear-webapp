import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { tracksCollection } from 'meteor/gear:collections';

Meteor.publish('products:trackForArtistId', function(artistId) {
  check(artistId, String);
  const cursor = tracksCollection.find({ 'artist._id': artistId }, { limit: 5 });
  return cursor;
});
