import { productsCollection, tracksCollection } from 'meteor/gear:collections';

import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';

Meteor.publishComposite('products:tracksForProductId', function(productId) {
  check(productId, String);
  const composition = {
    find: function() {
      return productsCollection.find({ _id: productId });
    },
    children: [
      {
        find: function(product) {
          return tracksCollection.find(
            {
              'artist.id': { $in: product.artistIds }
            },
            { limit: 5 }
          );
        }
      }
    ]
  };
  return composition;
});
