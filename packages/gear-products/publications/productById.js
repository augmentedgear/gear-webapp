import { Meteor } from 'meteor/meteor';
import { check } from 'meteor/check';
import { productsCollection } from 'meteor/gear:collections';

Meteor.publish('products:productById', function(_id) {
  check(_id, String);
  const cursor = productsCollection.find({ _id });
  return cursor;
});
