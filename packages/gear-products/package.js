Package.describe({ name: 'gear:products', version: '0.0.1', summary: 'Product pages' });

Package.onUse(function(api) {
  api.use(['templating', 'ecmascript', 'reactive-var', 'underscore']);
  api.use('fourseven:scss@4.5.4');
  api.use(['kadira:flow-router@2.11.0', 'kadira:blaze-layout@2.3.0']);
  api.use('arillo:flow-router-helpers@0.5.2');
  api.use('reywood:publish-composite@1.4.2');
  api.use('gear:collections');
  api.use('gear:analytics');

  addTemplates(api, ['guitar', 'product-feature', 'product-header']);
  api.addFiles(
    [
      'publications/productById.js',
      'publications/tracksForProductId.js',
      'publications/tracksForArtistId.js',
      'methods/getTracksForProductId.js'
    ],
    'server'
  );
  api.addFiles('routes/products.js');
});

function addTemplates(api, templates) {
  for (var t in templates) {
    var path = 'templates/' + templates[t];
    var files = [path + '.html', path + '.js', path + '.scss'];
    api.addFiles(files, 'client');
  }
}
