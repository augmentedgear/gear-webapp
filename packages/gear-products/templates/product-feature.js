import { FlowRouter } from 'meteor/kadira:flow-router';
import { Meteor } from 'meteor/meteor';
import { Template } from 'meteor/templating';
import { productsCollection } from 'meteor/gear:collections';

Template.productFeature.onCreated(function() {
  const instance = this;
  instance.autorun(() => {
    const productId = FlowRouter.getParam('productid');
    const part = FlowRouter.getParam('part');
    instance.subscribe('products:productById', productId);
    Meteor.call('analytics:trackAccess', 'guitars', productId, part);
  });
});

Template.productFeature.helpers({
  product() {
    const _id = FlowRouter.getParam('productid');
    return productsCollection.findOne({ _id });
  },
  feature() {
    const part = FlowRouter.getParam('part');
    return this[part];
  },
  property() {
    return Object.getOwnPropertyNames(this)[0];
  },
  value() {
    return this[Object.getOwnPropertyNames(this)[0]];
  }
});
