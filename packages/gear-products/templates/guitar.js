import { FlowRouter } from 'meteor/kadira:flow-router';
import { Meteor } from 'meteor/meteor';
import { ReactiveVar } from 'meteor/reactive-var';
import { Template } from 'meteor/templating';
import { productsCollection } from 'meteor/gear:collections';

Template.guitar.onCreated(function() {
  const instance = this;
  instance.tracks = new ReactiveVar();
  const productId = FlowRouter.getParam('productid');
  const part = FlowRouter.getParam('part');
  instance.subscribe('products:productById', productId);
  Meteor.call('product:tracksForProductId', productId, (error, result) => {
    if (error) {
      return;
    }
    instance.tracks.set(result);
  });
  Meteor.call('analytics:trackAccess', 'guitars', productId, part);
});

Template.guitar.helpers({
  product() {
    const product = productsCollection.findOne();
    return product;
  },
  tracklists() {
    const instance = Template.instance();
    const tracks = instance.tracks.get();
    if (!tracks) {
      return;
    }
    return tracks;
  },
  artist() {
    return this[0].artist;
  }
});
