import { BlazeLayout } from 'meteor/kadira:blaze-layout';
import { FlowRouter } from 'meteor/kadira:flow-router';

const productRoutes = FlowRouter.group({
  name: 'product',
  prefix: '/product'
});

productRoutes.route('/guitar/:productid/:part', {
  name: 'guitar',
  action: function() {
    const part = FlowRouter.getParam('part');
    const template = part === 'details' ? 'guitar' : 'productFeature';
    BlazeLayout.render('layout', { mainCanvas: template });
  }
});
