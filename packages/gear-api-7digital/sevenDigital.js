import { HTTP } from 'meteor/http';

const apiKey = '7dgtnpuecrvc';
// const secret = '9qwjmbxbk2qh76xu';
const shopId = 2020;
const baseUrl = 'http://api.7digital.com/1.2/';
const usageTypes = 'adsupportedstreaming';

class SevenDigital {
  getArtists(name) {
    const response = this.get({ endpoint: '/artist/browse', params: { letter: name } });
    return response.artists.artist;
  }
  getFirstArtist(name) {
    const artists = this.getArtists(name);
    if (artists.length === 0) {
      return {};
    }
    return artists[0];
  }
  getReleases(artistId) {
    const response = this.get({
      endpoint: '/artist/releases',
      params: { artistId, usageTypes }
    });
    return response.releases.releases;
  }
  getTrackListing(releaseId) {
    const response = this.get({
      endpoint: '/release/tracks',
      params: { releaseId, usageTypes }
    });
    return response.tracks;
  }
  get({ endpoint, params }) {
    const response = HTTP.get(`${baseUrl}${endpoint}`, {
      params: { ...params, oauth_consumer_key: apiKey, shopId },
      headers: {
        accept: 'application/json'
      }
    });
    if (response.statusCode !== 200) {
      return {};
    }
    return JSON.parse(response.content);
  }
}

const sevenDigital = new SevenDigital();

export default sevenDigital;
