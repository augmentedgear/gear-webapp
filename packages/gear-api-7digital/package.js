Package.describe({ name: 'gear:api-7digital', version: '0.0.1', summary: '7digital API' });

Package.onUse(function(api) {
  api.use(['ecmascript', 'http']);
  api.mainModule('main.server.js', 'server');
});
