import { Session } from 'meteor/session';
import { Template } from 'meteor/templating';
import { app } from 'meteor/gear:app-main';

//
// Global template helpers used troughout the app
//

// Get the global app object
Template.registerHelper('app', function() {
  return app;
});

// Get the instance of the current template
Template.registerHelper('instance', function() {
  return Template.instance();
});

// Get the template state
Template.registerHelper('getState', function(property) {
  return Template.instance().state.get(property);
});

// Log a string to the console
Template.registerHelper('log', function(s) {
  app.log(s);
});

// Log an object to the console
Template.registerHelper('dir', function(obj) {
  app.dir(obj);
});

// Device detection
Template.registerHelper('runningAsMobileApp', function() {
  return Session.get('runningAsMobileApp');
});

Template.registerHelper('runningOniOS', function() {
  return Session.get('runningOniOS');
});

Template.registerHelper('runningOnAndroid', function() {
  return Session.get('runningOnAndroid');
});

Template.registerHelper('runningOnMobile', function() {
  return Session.get('runningOnMobile');
});

Template.registerHelper('runningAsiOSApp', function() {
  return Session.get('runningAsMobileApp') && Session.get('runningOniOS');
});

Template.registerHelper('runningAsAndroidApp', function() {
  return Session.get('runningAsMobileApp') && Session.get('runningOnAndroid');
});

Template.registerHelper('packageInstalled', function(packageName) {
  return !!Package[packageName];
});
