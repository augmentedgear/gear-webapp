Package.describe({
  name: 'gear:app-main',
  version: '0.0.1',
  summary: 'Defines namespaces and global objects for the application'
});

Package.onUse(function(api) {
  api.use(['templating', 'ecmascript', 'session']);
  api.addFiles('namespaces.js');
  api.addFiles('global.helpers.js', 'client');
  api.export('app');
});
